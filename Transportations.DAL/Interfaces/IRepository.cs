﻿// <copyright file="IRepository.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.DAL.Interfaces
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// To increase the flexibility of connecting to a database
    /// </summary>
    /// <typeparam name="T">Repository Class</typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Get All data
        /// </summary>
        /// <returns>All rows</returns>
        IEnumerable<T> GetAll();
    }
}
