﻿// <copyright file="IUnitOfWork.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.DAL.Interfaces
{
    using System;
    using Entities;

    /// <summary>
    /// Interface Unit of work for easier communicate with database
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Gets transportations repository
        /// </summary>
        IRepository<Transportation> Transportations { get; }
    }
}
