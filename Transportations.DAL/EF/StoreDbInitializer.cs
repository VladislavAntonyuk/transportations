﻿// <copyright file="StoreDbInitializer.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.DAL.EF
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using Entities;

    /// <summary>
    /// Initialize database store
    /// </summary>
    public class StoreDbInitializer : DropCreateDatabaseIfModelChanges<TransportationContext>
    {
        /// <summary>
        /// Fill table
        /// </summary>
        /// <param name="db">Database context</param>
        protected override void Seed(TransportationContext db)
        {
            List<string> cities = new List<string>()
            {
                "Dnipro",
                "Kiev",
                "Lviv",
                "Zaporizha",
                "Herson",
                "Odessa",
                "Nikolaev",
                "Nikopol",
                "Kremenchug",
                "Borispol",
                "Vinnica",
                "Melitopol",
                "Pavlograd",
                "Yalta",
                "Ternopol",
                "Hmelnitskiy",
                "Djankoy",
                "Poltava",
                "Donetsk"
            };

            List<string> customers = new List<string>()
            {
                "Andrew",
                "Svetlana",
                "Vladislav",
                "Igor",
                "Oksana",
                "Alina",
                "Diana",
                "Nickolay",
                "Janna",
                "Anatoliy",
                "Kira",
                "Petr",
                "Tatyana",
                "Maxim",
                "Katya",
                "Miron",
                "Dima",
                "Inna",
                "Luba"
            };

            List<string> companies = new List<string>()
            {
                "Google",
                "Microsoft",
                "Apple",
                "Nokia",
                "HTC",
                "Samsung",
                "Blackberry",
                "Logitech",
                "Philips",
                "Panasonic",
                "Sony",
                "Asus",
                "Huawei",
                "Meizu",
                "Liebherr",
                "HP",
                "Intel",
                "Nvidia",
                "Daikin"
            };

            Random random = new Random();
            for (int i = 0; i < 400; i++)
            {
                int randomMonth = random.Next(1, 3);
                db.Transportations.Add(
                    new Transportation
                    {
                        Month = randomMonth,
                        Year = 2017,
                        CitySender = cities[random.Next(0, cities.Count - 1)],
                        CityTarget = cities[random.Next(0, cities.Count - 1)],
                        Customer = customers[random.Next(0, customers.Count - 1)],
                        Company = companies[random.Next(0, companies.Count - 1)],
                        PlannedTransportation = random.Next(100, 800),
                        ActualTransportation = random.Next(100, 800),
                        Day1 = 5,
                        Day2 = 10,
                        Day3 = 20,
                        Day4 = 30,
                        Day5 = 10,
                        Day6 = 5,
                        Day7 = 5,
                        Day10 = 100,
                        Day30 = (randomMonth == 2) ? null : (float?)random.Next(100, 800)
                    });
            }

            db.SaveChanges();
        }
    }
}