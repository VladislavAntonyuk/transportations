﻿// <copyright file="TransportationContext.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.DAL.EF
{
    using System.Data.Entity;
    using Entities;

    /// <summary>
    /// Transportation Context
    /// </summary>
    public class TransportationContext : DbContext
    {
        /// <summary>
        /// Initializes static members of the <see cref="TransportationContext"/> class
        /// </summary>
        static TransportationContext()
        {
            Database.SetInitializer(new StoreDbInitializer());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportationContext"/> class
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public TransportationContext(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// Gets or sets Collection of transportations
        /// </summary>
        public DbSet<Transportation> Transportations { get; set; }
    }
}   