﻿// <copyright file="TransportationRepository.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.DAL.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EF;
    using Entities;
    using Interfaces;

    /// <summary>
    /// Transportation Repository
    /// </summary>
    public class TransportationRepository : IRepository<Transportation>
    {
        /// <summary>
        /// Database Context
        /// </summary>
        private TransportationContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportationRepository"/> class
        /// </summary>
        /// <param name="context">Transportation Context</param>
        public TransportationRepository(TransportationContext context)
        {
            this.db = context;
        }

        /// <summary>
        /// Get all transportations
        /// </summary>
        /// <returns>List of transportations</returns>
        public IEnumerable<Transportation> GetAll()
        {
            return this.db.Transportations;
        }
    }
}
