﻿// <copyright file="EFUnitOfWork.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.DAL.Repositories
{
    using System;
    using EF;
    using Entities;
    using Interfaces;

    /// <summary>
    /// Uses for communicate with database
    /// </summary>
    public class EFUnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Database Context
        /// </summary>
        private TransportationContext db;

        /// <summary>
        /// Transportation Repository
        /// </summary>
        private TransportationRepository transportationRepository;

        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="EFUnitOfWork"/> class
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public EFUnitOfWork(string connectionString)
        {
            this.db = new TransportationContext(connectionString);
        }

        /// <summary>
        /// Gets transportation repository
        /// </summary>
        public IRepository<Transportation> Transportations
        {
            get
            {
                if (this.transportationRepository == null)
                {
                    this.transportationRepository = new TransportationRepository(this.db);
                }

                return this.transportationRepository;
            }
        }

        /// <summary>
        /// Implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">indicates whether the method call comes 
        /// from a Dispose method (its value is true) or from a finalize (its value is false).</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.db.Dispose();
                }

                this.disposed = true;
            }
        }

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
