﻿// <copyright file="ITransportationService.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.BLL.Interfaces
{
    using System.Collections.Generic;
    using DTO;

    /// <summary>
    /// Interfaces of transportation Service
    /// </summary>
    public interface ITransportationService
    {
        /// <summary>
        /// Get list of transportations
        /// </summary>
        /// <returns>list of transportations</returns>
        IEnumerable<TransportationDTO> GetTransportations();

        /// <summary>
        /// dispose database
        /// </summary>
        void Dispose();
    }
}
