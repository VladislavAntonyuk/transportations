﻿// <copyright file="TransportationService.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.BLL.Services
{
    using System.Collections.Generic;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Interfaces;

    /// <summary>
    /// Transportation service
    /// </summary>
    public class TransportationService : ITransportationService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransportationService"/> class
        /// </summary>
        /// <param name="uow">Unit of work</param>
        public TransportationService(IUnitOfWork uow)
        {
            this.Database = uow;
        }

        /// <summary>
        /// Gets or sets database
        /// </summary>
        public IUnitOfWork Database { get; set; }

        /// <summary>
        /// Get all transportations
        /// </summary>
        /// <returns>list of transportations</returns>
        public IEnumerable<TransportationDTO> GetTransportations()
        {
            //// use an automatic machine to project one collection to another
            Mapper.Initialize(cfg => cfg.CreateMap<Transportation, TransportationDTO>());
            return Mapper.Map<IEnumerable<Transportation>, List<TransportationDTO>>(this.Database.Transportations.GetAll());
        }
        
        /// <summary>
        /// Dispose database
        /// </summary>
        public void Dispose()
        {
            this.Database.Dispose();
        }
    }
}