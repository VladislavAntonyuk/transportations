﻿// <copyright file="ServiceModule.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.BLL.Inftastructure
{
    using DAL.Interfaces;
    using DAL.Repositories;
    using Ninject.Modules;

    /// <summary>
    /// Uses for dependency injection
    /// </summary>
    public class ServiceModule : NinjectModule
    {
        /// <summary>
        /// connection string
        /// </summary>
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceModule"/> class
        /// </summary>
        /// <param name="connection">connection string</param>
        public ServiceModule(string connection)
        {
            this.connectionString = connection;
        }

        /// <summary>
        /// Sets the use of Entity Framework unit of work as an interface unit of work
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EFUnitOfWork>().WithConstructorArgument(this.connectionString);
        }
    }
}
