﻿// <copyright file="TransportationViewModel.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.UI.Models
{
    /// <summary>
    /// Transportation View Model
    /// </summary>
    public class TransportationViewModel
    {
        /// <summary>
        /// Gets or sets Unique identifier
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets Transportation month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Gets or sets Transportation year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets sender city
        /// </summary>
        public string CitySender { get; set; }

        /// <summary>
        /// Gets or sets target city
        /// </summary>
        public string CityTarget { get; set; }

        /// <summary>
        /// Gets or sets person who ordered transportation
        /// </summary>
        public string Customer { get; set; }

        /// <summary>
        /// Gets or sets transportation company
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets planned weight
        /// </summary>
        public float PlannedTransportation { get; set; }

        /// <summary>
        /// Gets or sets received weight
        /// </summary>
        public float ActualTransportation { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day1 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day2 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day3 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day4 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day5 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day6 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day7 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day8 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day9 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day10 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day11 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day12 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day13 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day14 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day15 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day16 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day17 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day18 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day19 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day20 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day21 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day22 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day23 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day24 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day25 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day26 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day27 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float Day28 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float? Day29 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float? Day30 { get; set; }

        /// <summary>
        /// Gets or sets weight received on this day
        /// </summary>
        public float? Day31 { get; set; }
    }
}
