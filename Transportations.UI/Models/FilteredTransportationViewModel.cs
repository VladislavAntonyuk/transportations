﻿// <copyright file="FilteredTransportationViewModel.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.UI.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Filtered Transportation View Model
    /// </summary>
    public class FilteredTransportationViewModel
    {
        /// <summary>
        /// Gets or sets List of city targets
        /// </summary>
        public IEnumerable<string> CityTargets { get; set; }

        /// <summary>
        /// Gets or sets List of City senders
        /// </summary>
        public IEnumerable<string> CitySenders { get; set; }

        /// <summary>
        /// Gets or sets list of companies
        /// </summary>
        public IEnumerable<string> Companies { get; set; }

        /// <summary>
        /// Gets or sets list of customers
        /// </summary>
        public IEnumerable<string> Customers { get; set; }
    }
}