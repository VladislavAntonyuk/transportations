﻿// <copyright file="HomeController.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.UI.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using AutoMapper;
    using BLL.DTO;
    using BLL.Interfaces;
    using Models;

    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Transportation service
        /// </summary>
        private readonly ITransportationService transportationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class
        /// </summary>
        /// <param name="transportationService">Transportation Service</param>
        public HomeController(ITransportationService transportationService)
        {
            this.transportationService = transportationService;
        }

        /// <summary>
        /// Default page
        /// </summary>
        /// <returns>View of the</returns>
        public ActionResult Index()
        {
            IEnumerable<TransportationDTO> tr = this.transportationService.GetTransportations();
            Mapper.Initialize(cfg => cfg.CreateMap<TransportationDTO, TransportationViewModel>());
            var transportations = Mapper.Map<IEnumerable<TransportationDTO>, List<TransportationViewModel>>(tr);
            List<string> citySenders = new List<string>();
            List<string> cityTargets = new List<string>();
            List<string> companies = new List<string>();
            List<string> customers = new List<string>();
            foreach (var data in transportations)
            {
                citySenders.Add(data.CitySender);
                cityTargets.Add(data.CityTarget);
                companies.Add(data.Company);
                customers.Add(data.Customer);
            }

            FilteredTransportationViewModel filteredTransportaionViewModel = new FilteredTransportationViewModel()
            {
                CitySenders = citySenders,
                CityTargets = cityTargets,
                Companies = companies,
                Customers = customers
            };
            return this.View(filteredTransportaionViewModel);
        }

        /// <summary>
        /// Filter Search transportations
        /// </summary>
        /// <param name="month">Search month number</param>
        /// <param name="company">Array of selected companies</param>
        /// <param name="customer">Array of selected customers</param>
        /// <param name="cityTarget">Array of selected target cities</param>
        /// <param name="citySender">Array of selected sender cities</param>
        /// <returns>Filtered search transportations</returns>
        [HttpPost]
        public ActionResult Search(int? month, string[] company, string[] customer, string[] cityTarget, string[] citySender)
        {
            IEnumerable<TransportationDTO> tr = this.transportationService.GetTransportations().Where(x =>
                (company == null || company.Contains(x.Company))
                && (cityTarget == null || cityTarget.Contains(x.CityTarget))
                && (customer == null || customer.Contains(x.Customer))
                && (citySender == null || citySender.Contains(x.CitySender))
                && (month == null || x.Month == month));
            Mapper.Initialize(cfg => cfg.CreateMap<TransportationDTO, TransportationViewModel>());
            var transportations = Mapper.Map<IEnumerable<TransportationDTO>, List<TransportationViewModel>>(tr);
            return this.PartialView("TransportationList", transportations);
        }

        /// <summary>
        /// Display unfiltered table
        /// </summary>
        /// <returns>partial view Transportation List</returns>
        public ActionResult Search()
        {
            IEnumerable<TransportationDTO> tr = this.transportationService.GetTransportations();
            Mapper.Initialize(cfg => cfg.CreateMap<TransportationDTO, TransportationViewModel>());
            var transportations = Mapper.Map<IEnumerable<TransportationDTO>, List<TransportationViewModel>>(tr);
            return this.PartialView("TransportationList", transportations);
        }

        /// <summary>
        /// Dispose transportation service
        /// </summary>
        /// <param name="disposing">indicates whether the method call comes 
        /// from a Dispose method (its value is true) or from a finalize (its value is false).</param>
        protected override void Dispose(bool disposing)
        {
            this.transportationService.Dispose();
            base.Dispose(disposing);
        }
    }
}