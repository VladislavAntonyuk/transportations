﻿// <copyright file="NinjectDependencyResolver.cs" company="Vladislav Antonyuk">Created by Vladislav Antonyuk</copyright>

namespace Transportations.UI.Util
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using BLL.Interfaces;
    using BLL.Services;
    using Ninject;

    /// <summary>
    /// Dependency Mapper class
    /// </summary>
    public class NinjectDependencyResolver : IDependencyResolver
    {
        /// <summary>
        /// Kernel variable
        /// </summary>
        private IKernel kernel;

        /// <summary>
        /// Initializes a new instance of the <see cref="NinjectDependencyResolver"/> class
        /// </summary>
        /// <param name="kernelParam">Kernel parameter</param>
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            this.kernel = kernelParam;
            this.AddBindings();
        }

        /// <summary>
        /// Get service
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <returns>Service object</returns>
        public object GetService(Type serviceType)
        {
            return this.kernel.TryGet(serviceType);
        }

        /// <summary>
        /// Get all services
        /// </summary>
        /// <param name="serviceType">Service type</param>
        /// <returns>List of services</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.kernel.GetAll(serviceType);
        }

        /// <summary>
        /// Matched interface with implementation
        /// </summary>
        private void AddBindings()
        {
            this.kernel.Bind<ITransportationService>().To<TransportationService>();
        }
    }
}